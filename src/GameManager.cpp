#include <ncurses.h>
#include <unistd.h>
#include <ctime>
#include <chrono>

#include "GameManager.h"
#include "GameState.h"
#include "IntroState.h"

#define DELAY 10000

GameManager::GameManager() {
    _random_engine.seed(time(0));

    player = enemy = nullptr;
}

void GameManager::init() {

    initAssets();

    _isRunning = true;
    wave = 0;

    pushState(IntroState::instance());

    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);

    /*std::uniform_int_distribution<int> snowDistribution(0, max_x);
    for (int i = 0; i < max_x * 0.02; ++i) {
        snow.push_back(Point2d {snowDistribution(_random_engine), 0});
    }*/
}

void GameManager::initAssets() {
    assetManager = AssetManager::instance();

    assetManager->addAsset("shield", new ASCIIAsset("../res/graphics/shield.txt"));
    assetManager->addAsset("arms", new ASCIIAsset("../res/graphics/arms.txt"));
    assetManager->addAsset("helmet", new ASCIIAsset("../res/graphics/helmet.txt"));
    assetManager->addAsset("logo", new ASCIIAsset("../res/graphics/logo.txt"));
    assetManager->addAsset("preparation_logo", new ASCIIAsset("../res/graphics/preparation_logo.txt"));
    assetManager->addAsset("battle_logo", new ASCIIAsset("../res/graphics/battle_logo.txt"));
    assetManager->addAsset("evaluation_logo", new ASCIIAsset("../res/graphics/evaluation_logo.txt"));
}

void GameManager::start() {
    std::chrono::steady_clock::time_point currentTime;
    std::chrono::steady_clock::time_point lastTime;
    long delta;

    lastTime = std::chrono::steady_clock::now();

    /*
    std::chrono::steady_clock::time_point now;
    std::chrono::steady_clock::time_point snowfall_timer;

    now = std::chrono::steady_clock::now();
    snowfall_timer = std::chrono::steady_clock::now();
    */

    while(_isRunning) {

        currentTime = std::chrono::steady_clock::now();
        delta = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - lastTime).count();

        processInput();
        update(delta);
        draw();

        usleep(DELAY);
        lastTime = currentTime;

        /*
        std::vector<Point2d>::iterator it;
        for (it = snow.begin(); it != snow.end(); ++it) {
            mvprintw(it->y, it->x, "*");
        }

        if (std::chrono::duration_cast<std::chrono::milliseconds>(now - snowfall_timer).count() > 100) {
            std::uniform_int_distribution<int> snowDistribution(0, max_x);
            for (int i = 0; i < max_x * 0.02; ++i) {
                snow.push_back(Point2d {snowDistribution(random_engine), 0});
            }
            for (it = snow.begin(); it != snow.end(); ++it) {
                mvprintw(it->y, it->x, "*");
                it->y++;
                if (it->y > max_y) {
                    it = snow.erase(it);
                }
            }
            snowfall_timer = std::chrono::steady_clock::now();
        }
        */
    }
}

void GameManager::shutdown() {
    // cleanup remaining states on the stack
    while ( !_states.empty() ) {
        _states.back()->cleanup();
        _states.pop_back();
    }

    // delete pointers
    delete player;
    delete enemy;
}

void GameManager::processInput() {
    _states.back()->processInput();
}

void GameManager::update(long delta) {
    _states.back()->update(delta);
}

void GameManager::draw() {
    _states.back()->draw();
}

void GameManager::changeState(GameState *state) {
    // cleanup current state
    if ( !_states.empty() ) {
        _states.back()->cleanup();
        _states.pop_back();
    }

    // add and init new state
    _states.push_back(state);
    _states.back()->init(this);
}

void GameManager::pushState(GameState *state) {
    // pause current state
    if ( !_states.empty() ) {
        _states.back()->pause();
    }

    // add and init new state
    _states.push_back(state);
    _states.back()->init(this);
}

void GameManager::popState() {
    // cleanup the current state
	if ( !_states.empty() ) {
		_states.back()->cleanup();
		_states.pop_back();
	}

	// resume previous state
	if ( !_states.empty() ) {
		_states.back()->resume();
	}
}

