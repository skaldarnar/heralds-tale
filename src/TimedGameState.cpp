#include "TimedGameState.h"

void TimedGameState::init(GameManager* manager) {
    GameState::init(manager);
    _time_elapsed = 0;
}

void TimedGameState::update(long delta) {
    _time_elapsed += delta;
}
