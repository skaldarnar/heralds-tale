#include "IntroState.h"

#include <ncurses.h>
#include "util.hpp"
#include "PlayState.h"

#define KEY_ESC 27
#define WAITING_TIME 2000

void printStringCentered(int r, int c, std::string str);

IntroState IntroState::_instance;

const std::string IntroState::_subtitle = "F O R S A K E N   G U A R D S";

IntroState::IntroState() { }

void IntroState::init(GameManager* gameManager) {
    _gameManager = gameManager;

    AssetManager* assetManager = AssetManager::instance();
    _logo = assetManager->getAsset("logo");
    _shield = assetManager->getAsset("shield");
    _time_elapsed = 0;
}

void IntroState::cleanup() {

}

void IntroState::pause() {

}

void IntroState::resume() {

}

void IntroState::processInput() {
    int ch;
    nodelay(stdscr, TRUE);
    if ((ch = getch()) != ERR) {
        switch(ch) {
        case KEY_F(1):
        case KEY_EXIT:
        case KEY_ESC:
            _gameManager->quit();
            break;
        default:
            if (_time_elapsed > WAITING_TIME) {
                _gameManager->changeState(PlayState::instance());
            }
            break;
        }
    }
}

void IntroState::update(long time) {
    _time_elapsed += time;

}

void IntroState::draw() {
    int max_x, max_y;
    int logo_x, logo_y;
    getmaxyx(stdscr, max_y, max_x);

    clear();

    logo_x = max_x/2;
    logo_y = max_y/2 - 0.2f*max_y;

    _logo->drawCentered(  logo_y, logo_x);
    _shield->drawCentered(logo_y, logo_x - _logo->getWidth()/2 - 1.2f * _shield->getWidth());
    _shield->drawCentered(logo_y, logo_x + _logo->getWidth()/2 + 1.2f * _shield->getWidth());

    mvprintw(logo_y + _logo->getHeight()/2 + 4, max_x/2 - _subtitle.size()/2, getSubtitle().c_str());

    if (_time_elapsed > WAITING_TIME) {
        util::printPressAnyKey(logo_y + 0.6*max_y, logo_x);
    }

    refresh();
}

std::string IntroState::getSubtitle() {
    if (_time_elapsed < 500) {
        return "";
    }
    return _subtitle.substr(0, (_time_elapsed-500) / 50);
}
