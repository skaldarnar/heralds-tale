#include "Soldier.h"
#include <ctime>

std::mt19937 Soldier::_random_engine(time(NULL));

Soldier::Soldier()
    : _health(100), _damage(10), _chanceForHit(0.5f), _chanceForCritical(0.01f) { }

void Soldier::init(int health, int damage, float chanceForHit, float chanceForCritical) {
    _health = health;
    _damage = damage;
    _chanceForHit = chanceForHit;
    _chanceForCritical = chanceForCritical;
    _num_trainings = 0;
}

float Soldier::attack() {
    float attack;
    int multiplier = 1;
    std::uniform_real_distribution<float> attackRoll(0.0f, 1.0f);

    attack = attackRoll(_random_engine);
    if (attack < _chanceForHit) {
        multiplier = (attack < _chanceForCritical) ? 2 : 1;
        return _damage * multiplier;
    }
    return 0;
}

int Soldier::trainingCost() {
    return pow(1.1f, _num_trainings) * INITIAL_TRAINING_COST;
}

void Soldier::increaseChanceForHit(float value) {
    _chanceForHit += value;
}

void Soldier::increaseChanceForCritical(float value) {
    _chanceForCritical += value;
}

void Soldier::increaseDamage(int value) {
    _damage += value;
    _num_trainings++;
}

void Soldier::increaseHealth(int value) {
    _health += value;
}
