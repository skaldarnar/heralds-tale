#include "PlayState.h"
#include "PreparationState.h"
#include "EvaluationState.h"

#define KEY_ESC 27

PlayState PlayState::_instance;

PlayState::PlayState()
{
    //ctor
}

void PlayState::init(GameManager* gameManager) {
    GameState::init(gameManager);

    // set up player gold
    _gameManager->playerGold = 300;

    // set up player army
    Soldier *playerSoldier = new Soldier();
    Army* playerArmy = new Army();
    playerSoldier->init(100, 10, 0.4f, 0.01f);
    playerArmy->init(100, playerSoldier);
    _gameManager->player = playerArmy;

    // set up enemy army
    Soldier *enemySoldier = new Soldier();
    Army *enemyArmy = new Army();
    enemySoldier->init(50, 5, 0.3f, 0.0f);
    enemyArmy->init(200, enemySoldier);
    _gameManager->enemy  = enemyArmy;

    // go into the preparation phase
    _gameManager->pushState(PreparationState::instance());
}

void PlayState::resume() {
    _gameManager->changeState(EvaluationState::instance());
}

void PlayState::processInput() {

}

void PlayState::update(long delta) { }

void PlayState::draw() {
    clear();
}
