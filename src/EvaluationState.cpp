#include "EvaluationState.h"
#include "util.hpp"

#define WAITING_TIME 2000

EvaluationState EvaluationState::_instance;

void EvaluationState::init(GameManager *gameManager) {
    TimedGameState::init(gameManager);

    _logo = AssetManager::instance()->getAsset("evaluation_logo");
}

void EvaluationState::processInput() {
    int ch;
    if ((ch = getch()) != ERR) {
        switch(ch) {
        default:
            if (_time_elapsed > WAITING_TIME) {
                _gameManager->quit();
            }
            break;
        }
    }
}

void EvaluationState::draw() {
    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);

    clear();

    _logo->drawCentered(0.1f*max_y, max_x/2);
    attron(A_BOLD);
    if (_gameManager->player->getNumSoldiers() > 0) {
        util::printString(max_y/2, max_x/2, "You have defeated the enemy!");
    } else {
        util::printString(max_y/2, max_x/2, "You have lost the battle!");
    }
    attroff(A_BOLD);

    if (_time_elapsed > WAITING_TIME) {
        util::printPressAnyKey(0.8*max_y, max_x/2);
    }
    refresh();
}
