#include "PreparationState.h"
#include "BattleState.h"
#include "util.hpp"
#include <sstream>
#include <iomanip>

#define INITIAL_ENEMY_SOLDIERS 100
#define INITIAL_ENEMY_DAMAGE 5
#define INITIAL_ENEMY_HEALTH 50
#define WAVES_TO_FIGHT 5

PreparationState PreparationState::_instance;

std::mt19937 PreparationState::_random_engine(time(NULL));

PreparationState::PreparationState() {}

void PreparationState::init(GameManager* gameManager) {
    GameState::init(gameManager);

    _gameManager->wave++;
    if (_gameManager->wave > WAVES_TO_FIGHT) {
        _gameManager->popState();
    }
    _gameManager->enemy->init(getStrengthForWave(_gameManager->wave), getSoldierForWave(_gameManager->wave));

    _logo = AssetManager::instance()->getAsset("preparation_logo");
    _helmet = AssetManager::instance()->getAsset("helmet");
}

void PreparationState::processInput() {
    int ch;

    int cost;
    Army* player = _gameManager->player;

    if ((ch = getch()) != ERR) {
        switch(ch) {
        case 't':
        case 'T':
            cost = player->getSoldier()->trainingCost();
            if (_gameManager->playerGold >= cost) {
                player->getSoldier()->increaseDamage(2);
                _gameManager->playerGold -= cost;
            }
            break;
        case 'r':
        case 'R':
            cost = player->recruitementCost();
            if (_gameManager->playerGold >= cost) {
                player->increaseNumSoldiers(25);
                _gameManager->playerGold -= cost;
            }
            break;
        case 'b':
        case 'B':
            _gameManager->changeState(BattleState::instance());
            break;
        case KEY_F(1):
        case 27:
            _gameManager->quit();
            break;
        default:
            break;
        }
    }

}

void PreparationState::draw() {
    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);
    int row = 0.1f * max_y;

    clear();

    _helmet->drawCentered(row, 0.1f*max_x);
    _logo->drawCentered(row, max_x/2);
    _helmet->drawCentered(row, 0.9f*max_x);

    printOptions();
    printStatus();

    refresh();
}

void PreparationState::printStatus() {
    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);
    int row = 0.9f*max_y;

    std::stringstream str;

    str << "Damage: " << std::setw(3) << _gameManager->player->getSoldier()->getDamage();
    util::printString(row, 0.1f*max_x, str.str(), util::LEFT);

    str.str( std::string() );
    str.clear();

    str << "Gold: " << std::setw(4) << _gameManager->playerGold;
    util::printString(row, max_x/2, str.str(), util::CENTER);

    str.str( std::string() );
    str.clear();

    str << "Soldiers: " << std::setw(4) << _gameManager->player->getNumSoldiers();
    util::printString(row, 0.9f*max_x, str.str(), util::RIGHT);
}

void PreparationState::printOptions() {
    printOption(0, 'r', "recruit soldiers", "25 men at arms"      , _gameManager->player->recruitementCost());
    printOption(3, 't', "train soldiers"  , "increase damage by 2", _gameManager->player->getSoldier()->trainingCost());
    printOption(6, 'b', "go to battle");
}

void PreparationState::printOption(int offset, char key, std::string title, std::string expl, int cost) {
    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);

    int row = 0.4f * max_y + offset;
    int indent = 4;

    printOption(offset, key, title);

    std::stringstream stream;

    stream << "[" << std::setw(3) << cost << "Gold]";
    util::printString(row, 0.85f*max_x, stream.str(), util::RIGHT);

    stream.str( std::string() );

    stream << "(" << expl << ")";
    util::printString(row + 1, 0.15f*max_x+indent, stream.str(), util::LEFT);
}

void PreparationState::printOption(int offset, char key, std::string title) {
    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);

    int row = 0.4f * max_y + offset;

    std::stringstream stream;
    stream << key << ") " << title;
    util::printString(row, 0.15f*max_x, stream.str(), util::LEFT);
}

int PreparationState::getStrengthForWave(int wave) {
    int lower = 0.8f * wave * INITIAL_ENEMY_SOLDIERS;
    int upper = 1.9f * wave * INITIAL_ENEMY_SOLDIERS;

    std::uniform_int_distribution<int> distribution(lower, upper);
    return distribution(_random_engine);
}

Soldier* PreparationState::getSoldierForWave(int wave) {
    int lower_damage = pow(1.05f, wave) * INITIAL_ENEMY_DAMAGE;
    int upper_damage = pow(1.20f, wave) * INITIAL_ENEMY_DAMAGE;

    int lower_health = pow(1.05f, wave) * INITIAL_ENEMY_HEALTH;
    int upper_health = pow(1.20f, wave) * INITIAL_ENEMY_HEALTH;

    std::uniform_int_distribution<int> damage_distribution(lower_damage, upper_damage);
    std::uniform_int_distribution<int> health_distribution(lower_health, upper_health);

    Soldier *s = new Soldier();
    s->init(health_distribution(_random_engine), damage_distribution(_random_engine), 0.4f, 0.01f);

    return s;
}

