#include "MenuState.h"

#include "Utility.h"

#define KEY_SPACE ' '
#define KEY_RETURN 0xa

MenuState MenuState::_instance;

std::string MenuState::_menu[] = { "Start", "Quit" };

MenuState::MenuState()
{
    //ctor
}

void MenuState::init() {

}

void MenuState::cleanup() {

}

void MenuState::processInput(GameManager* gameManager) {
    int ch;
    nodelay(stdscr, TRUE);
    keypad(stdscr, TRUE);
    if((ch = getch()) != ERR) {
        switch(ch) {
        case KEY_DOWN:
        case 'j':
            _current_index = (_current_index + 1) % 2;
            break;
        case KEY_UP:
        case 'k':
            _current_index = ((_current_index - 1) % 2 + 2) % 2;
            break;
        case KEY_SPACE:
        case KEY_ENTER:
        case KEY_RETURN:
            if (_current_index == 1) {
                gameManager->quit();
            }
        }
    }

}

void MenuState::update(GameManager* gameManager, long time) {

}

void MenuState::draw(GameManager* gameManager) {
    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);

    int location_x = max_x/2;

    clear();
    for (int i = 0; i < 2; i++) {
        if (_current_index == i) attron(A_STANDOUT);
        Utility::printStringCentered(i, location_x, _menu[i]);
        if (_current_index == i) attroff(A_STANDOUT);
    }
    refresh();
}
