#include "BattleState.h"

#include <sstream>
#include <iostream>
#include <iomanip>
#include "util.hpp"
#include "PreparationState.h"

BattleState BattleState::_instance;

std::mt19937 BattleState::_random_engine(time(NULL));

BattleState::BattleState() { }

void BattleState::init(GameManager* manager) {
    TimedGameState::init(manager);

    AssetManager* assetManager;
    assetManager = AssetManager::instance();
    _arms = assetManager->getAsset("arms");
    _logo = assetManager->getAsset("battle_logo");

    _initialPlayerSoldiers = _gameManager->player->getNumSoldiers();
    _initialEnemySoldiers  = _gameManager->enemy->getNumSoldiers();

    std::uniform_int_distribution<int> distribution(0, 1);
    _turn = Faction(distribution(_random_engine));
}

void BattleState::cleanup() {

}

void BattleState::processInput() {
    int ch;
    if ((ch = getch()) != ERR) {
        switch(ch) {
        case KEY_F(1):
        case 27:
            _gameManager->quit();
            break;
        }
    }
}

void BattleState::update(long delta) {
    TimedGameState::update(delta);

    Army *p, *e;
    p = _gameManager->player;
    e = _gameManager->enemy;


    for (int i = 0; i < delta; i++) {
        if (p->getNumSoldiers() < 0) {
            _gameManager->popState();
            break;
        } else if (e->getNumSoldiers() < 0) {
            _gameManager->playerGold += _initialEnemySoldiers;
            _gameManager->enemy->setNumSoldiers(100);
            _gameManager->changeState(PreparationState::instance());
            break;
        }

        if (_turn == PLAYER) {
            simulateBattle(p, e);
            _turn = ENEMY;
        } else {
            simulateBattle(e, p);
            _turn = PLAYER;
        }
    }
}

void BattleState::draw() {
    int max_x, max_y;
    getmaxyx(stdscr, max_y, max_x);

    std::stringstream p, e;
    p << std::setw(4) << _gameManager->player->getNumSoldiers() << "/" << std::setw(4) << _initialPlayerSoldiers;
    e << std::setw(4) << _gameManager->enemy->getNumSoldiers()  << "/" << std::setw(4) << _initialEnemySoldiers;

    clear();

    _arms->drawCentered(max_y/2, max_x/2);
    _logo->drawCentered(0.1f*max_y, max_x/2);

    util::printString(0.8f*max_y  ,   max_x/4, p.str().c_str());
    util::printString(0.8f*max_y+1,   max_x/4, "your troops");
    util::drawBar(0.2f*max_y, max_x/4, 0.5f*max_y, (float)_gameManager->player->getNumSoldiers()/_initialPlayerSoldiers);
    util::printString(0.8f*max_y  , 3*max_x/4, e.str().c_str());
    util::printString(0.8f*max_y+1, 3*max_x/4, "enemy troops");
    util::drawBar(0.2f*max_y, 3*max_x/4, 0.5f*max_y, (float)_gameManager->enemy->getNumSoldiers()/_initialEnemySoldiers);

    refresh();
}

void BattleState::simulateBattle(Army *attacker, Army *defender) {
    int attack = attacker->getSoldier()->attack();

    defender->decreaseCurrentHealth(attack);
    if (defender->getCurrentHealth() < 0) {
        defender->killSoldier();
        defender->resetCurrentHealth();
    }
}
