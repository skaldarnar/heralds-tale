#include <ncurses.h>
#include "GameManager.h"
#include "GameState.h"

void init();
void shutdown();

int main() {

    init();

    GameManager manager;

    manager.init();
    manager.start();
    manager.shutdown();

    shutdown();

    return 0;
}

void init() {
    initscr();
    raw();
    noecho();
    curs_set(FALSE);
}

void shutdown() {
    endwin();
}
