#include "util.hpp"

void util::printString(int r, int c, std::string str, Justification justification) {
    int col;
    switch(justification) {
    case LEFT:
        col = c;
        break;
    case CENTER:
        col = c - str.size()/2;
        break;
    case RIGHT:
        col = c - str.size();
        break;
    }
    mvprintw(r, col, str.c_str());
}

void util::printPressAnyKey(int r, int c) {
    printString(r, c, "Press any key to continue ...", CENTER);
}

void util::drawBar(int r, int c, int h, float p, int w) {
    // clear the bar
    for (int i = 0; i < h; ++i) {
        printString(r+i, c, std::string(w, ' '));
    }
    // fill the bar bottom-up
    for (int i = 0; i < p*h; ++i) {
        printString(r+h-i, c, std::string(w, '#'));
    }
    refresh();
}
