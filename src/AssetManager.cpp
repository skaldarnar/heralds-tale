#include "AssetManager.h"

AssetManager AssetManager::_instance;

ASCIIAsset *AssetManager::getAsset(std::string key) {
    if (_assets.count(key)) {
        return _assets[key];
    }
    return nullptr;
}

void AssetManager::addAsset(std::string key, ASCIIAsset* asset) {
    _assets[key] = asset;
}
