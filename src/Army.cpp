#include "Army.h"

Army::Army()
    : _numSoldiers(0), _currentHealth(0), _soldier(nullptr) { }

void Army::init(int numSoldiers, Soldier* soldier) {
    _numSoldiers = numSoldiers;
    _soldier = soldier;
    _currentHealth = _soldier->getHealth();
    _num_recruitements = 0;
}

void Army::increaseNumSoldiers(int value) {
    _numSoldiers += value;
    _num_recruitements++;
}

void Army::decreaseCurrentHealth(int value) {
    _currentHealth -= value;
}

int Army::recruitementCost() const {
    return pow(1.1f, _num_recruitements) * INITIAL_RECRUITEMENT_COST;
}
