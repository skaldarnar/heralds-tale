#include "ASCIIAsset.h"

ASCIIAsset::ASCIIAsset(std::string fileName) {
    std::ifstream inputFile;
    inputFile.open(fileName);

    if (inputFile.fail()) {
        perror(fileName.c_str());
        _width = _height = 0;
        return;
    }

    std::string line;
    _width = 0;

    while (getline(inputFile, line)) {
        _lines.push_back(line);

        _width = (line.size() > _width) ? line.size() : _width;
    }
    _height = _lines.size();

    inputFile.close();

}

ASCIIAsset::~ASCIIAsset() {
    _lines.clear();
}

void ASCIIAsset::draw(int r, int c) {
    for (int i = 0; i < _lines.size(); i++) {
        mvprintw(r+i, c, _lines[i].c_str());
    }
}
