#ifndef _HERALDS_TALE_BATTLESTATE_H_
#define _HERALDS_TALE_BATTLESTATE_H_

#include "TimedGameState.h"
#include "AssetManager.h"
#include <random>
#include <ctime>

class BattleState : public TimedGameState
{
    private:
        static BattleState _instance;
        static std::mt19937 _random_engine;

        enum Faction {PLAYER, ENEMY};

        ASCIIAsset *_arms, *_logo;
        int _initialPlayerSoldiers, _initialEnemySoldiers;
        Faction _turn;

    public:
        static BattleState* instance() { return &_instance; }

        void init(GameManager* manager);
        void cleanup();

        void processInput();
        void update(long delta);
        void draw();


    protected:
        BattleState();

        void simulateBattle(Army *a1, Army *a2);
};

#endif // _HERALDS_TALE_BATTLESTATE_H_
