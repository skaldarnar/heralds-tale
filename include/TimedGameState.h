#ifndef _HERALDS_TALE_TIMEDGAMESTATE_H_
#define _HERALDS_TALE_TIMEDGAMESTATE_H_

#include "GameState.h"


class TimedGameState : public GameState
{
    protected:
        long _time_elapsed;;

    public:
        virtual void init(GameManager* manager);
        virtual void update(long delta);


};

#endif // _HERALDS_TALE_TIMEDGAMESTATE_H_
