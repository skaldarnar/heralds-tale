#ifndef _HERALDS_TALE_GAMESTATE_H_
#define _HERALDS_TALE_GAMESTATE_H_

#include "GameManager.h"

#include <ncurses.h>
#include <string>

class GameState {

protected:
    GameManager* _gameManager;

public:
    virtual void init(GameManager *gameManager) { _gameManager = gameManager; }
    virtual void cleanup() = 0;

    virtual void pause() {};
    virtual void resume() {};

    virtual void processInput() = 0;
    virtual void update(long time) = 0;
    virtual void draw() = 0;

protected:
    GameState() {}

};

#endif // _HERALDS_TALE_GAMESTATE_H_
