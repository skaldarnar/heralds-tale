#ifndef _HERALDS_TALE_ASCIIASSET_H_
#define _HERALDS_TALE_ASCIIASSET_H_

#include <string>
#include <vector>
#include <ncurses.h>
#include <fstream>

class ASCIIAsset
{
    private:
        std::vector<std::string> _lines;
        int _width;
        int _height;

    public:
        ASCIIAsset(std::string fileName);
        virtual ~ASCIIAsset();

        virtual void draw(int r, int c);
        void drawCentered(int r, int c) { draw(r - _height/2, c - _width/2); }

        // Getters
        int getWidth() { return _width; }
        int getHeight() { return _height; }

};

#endif // _HERALDS_TALE_ASCIIASSET_H_
