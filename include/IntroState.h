#ifndef _HERALDS_TALE_INTROSTATE_H_
#define _HERALDS_TALE_INTROSTATE_H_

#include "GameState.h"
#include "AssetManager.h"

#include <string>

class IntroState : public GameState {

private:
    static IntroState _instance;
    static const std::string _subtitle;

    ASCIIAsset *_logo, *_shield;
    int _time_elapsed;

public:
    static IntroState* instance() { return &_instance; }

    void init(GameManager* gameManager);
    void cleanup();

    void pause();
    void resume();

    void processInput();
    void update(long time);
    void draw();

protected:
    IntroState();

private:
    std::string getSubtitle();

};

#endif // _HERALDS_TALE_INTROSTATE_H_
