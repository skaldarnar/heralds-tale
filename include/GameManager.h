#ifndef _HERALDS_TALE_GAMEMANAGER_H_
#define _HERALDS_TALE_GAMEMANAGER_H_

#include "AssetManager.h"
#include "Army.h"
#include <vector>
#include <random>

class GameState;

class GameManager {

private:
    struct Point2d {
        int x, y;
    };

    AssetManager* assetManager;
    std::mt19937 _random_engine;
    std::vector<Point2d> snow;

    bool _isRunning;
    std::vector<GameState*> _states;

public:
    GameManager();
    virtual ~GameManager() {};

    // directly accessible variables
    Army *player, *enemy;
    int playerGold;
    int wave;

    // control the game manager
    void init();
    void start();
    void shutdown();

    // state management
    void changeState(GameState *state);
    void pushState(GameState *state);
    void popState();

    bool isRunning() const { return _isRunning; }
    void quit() { _isRunning = false; }

private:
    // internal initialization
    void initAssets();

    // game loop
    void processInput();
    void update(long elapsed);
    void draw();
};


#endif //_HERALDS_TALE_GAMEMANAGER_H_
