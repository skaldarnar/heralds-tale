#ifndef _HERALDS_TALE_SOLDIER_H_
#define _HERALDS_TALE_SOLDIER_H_

#include <random>

class Soldier {

protected:
    static std::mt19937 _random_engine;

    static const int INITIAL_TRAINING_COST = 25;

    int _health;
    int _damage;

    float _chanceForHit;
    float _chanceForCritical;

    int _num_trainings;

public:
    Soldier();

    void init(int health, int damage, float chanceForHit, float chanceForCritical);

    // Getters
    int getHealth() const { return _health; }
    int getDamage() const { return _damage; }
    float getChanceForHit() const { return _chanceForHit; }
    float getChanceForCritical() const { return _chanceForCritical; }

    // Modifiers
    void increaseChanceForHit(float value);
    void increaseChanceForCritical(float value);
    void increaseDamage(int value);
    void increaseHealth(int value);

    float attack();
    int trainingCost();
};


#endif //_HERALDS_TALE_SOLDIER_H_
