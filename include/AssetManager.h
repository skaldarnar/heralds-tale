#ifndef _HERALDS_TALE_ASSETMANAGER_H_
#define _HERALDS_TALE_ASSETMANAGER_H_


#include <ASCIIAsset.h>
#include <string>
#include <map>

class AssetManager {

private:
    static AssetManager _instance;
    std::map<std::string, ASCIIAsset*> _assets;

public:
    static AssetManager* instance() { return &_instance; }

    ASCIIAsset* getAsset(std::string key);
    void addAsset(std::string key, ASCIIAsset* asset);

private:
    AssetManager() {};
    AssetManager(const AssetManager&);

};


#endif //_HERALDS_TALE_ASSETMANAGER_H_
