#ifndef _HERALDS_TALE_PREPARATIONSTATE_H_
#define _HERALDS_TALE_PREPARATIONSTATE_H_

#include "GameState.h"
#include "ASCIIAsset.h"
#include "AssetManager.h"

class PreparationState : public GameState
{
    private:
        static PreparationState _instance;
        static std::mt19937 _random_engine;

        ASCIIAsset *_logo, *_helmet;

    public:
        static PreparationState* instance() { return &_instance; }

        void init(GameManager*);
        void cleanup() {}

        void processInput();
        void update(long delta) {};
        void draw();

    protected:
        PreparationState();

    private:
        void printStatus();
        void printOptions();
        void printOption(int offset, char key, std::string title, std::string expl, int cost);
        void printOption(int offset, char key, std::string title);

        // utility
        int getStrengthForWave(int wave);
        Soldier* getSoldierForWave(int wave);
};

#endif // _HERALDS_TALE_PREPARATIONSTATE_H_
