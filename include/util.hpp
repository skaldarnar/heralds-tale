#ifndef UTILITY_H
#define UTILITY_H

#include <string>
#include <ncurses.h>

namespace util {
    enum Justification { LEFT, CENTER, RIGHT };

    void printString(int r, int c, std::string str, Justification justification=CENTER);

    void printPressAnyKey(int r, int c);

    void drawBar(int row, int col, int height, float percentage, int width=3);
};

#endif // UTILITY_H
