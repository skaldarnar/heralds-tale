#ifndef _HERALDS_TALE_PLAYSTATE_H_
#define _HERALDS_TALE_PLAYSTATE_H_

#include "GameState.h"

#include "Army.h"
#include "Soldier.h"


class PlayState : public GameState
{
    private:
        static PlayState _instance;

    public:
        static PlayState* instance() { return &_instance; }

        void init(GameManager* gameManager);
        void cleanup() { };

        void resume();

        void processInput();
        void update(long delta);
        void draw();

    protected:
        PlayState();

    private:
};

#endif // _HERALDS_TALE_PLAYSTATE_H_
