#ifndef _HERALDS_TALE_EVALUATIONSTATE_H_
#define _HERALDS_TALE_EVALUATIONSTATE_H_

#include "TimedGameState.h"

class EvaluationState : public TimedGameState
{
    private:
        static EvaluationState _instance;

        ASCIIAsset *_logo;

    public:
        static EvaluationState* instance() { return &_instance; }

        void init(GameManager *gameManager);
        void cleanup() {}

        void processInput();
        void draw();

    protected:
        EvaluationState() {};

    private:
};

#endif // _HERALDS_TALE_EVALUATIONSTATE_H_
