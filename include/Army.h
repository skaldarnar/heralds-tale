#ifndef _HERALDS_TALE_ARMY_H_
#define _HERALDS_TALE_ARMY_H_


#include "Soldier.h"

class Army {

protected:
    static const int INITIAL_RECRUITEMENT_COST = 35;

    Soldier* _soldier;
    int _numSoldiers;
    int _currentHealth;

    int _num_recruitements;

public:
    Army();
    ~Army() { delete _soldier; }

    void init(int numSoldiers, Soldier *soldier);

    // Modifiers
    void increaseNumSoldiers(int value);
    void killSoldier() { _numSoldiers--; }
    void decreaseCurrentHealth(int value);
    void resetCurrentHealth() { _currentHealth = _soldier->getHealth(); }

    // Setters
    void setNumSoldiers(int numSoldiers) { _numSoldiers = numSoldiers; }

    // Getters
    Soldier* getSoldier() const { return _soldier; }
    int getNumSoldiers() const { return _numSoldiers; }
    int getCurrentHealth() const { return _currentHealth; }

    int recruitementCost() const;

};


#endif //_HERALDS_TALE_ARMY_H_
